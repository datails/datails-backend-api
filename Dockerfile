FROM node:14.9.0-alpine3.10

ENV PM2_PUBLIC_KEY oi35vogm6gs7lxi
ENV PM2_SECRET_KEY shbnp5bxiw7yh1p

# docker workdir
WORKDIR /home/usr/app

# copy files
COPY . .

# build the app
RUN npm ci && npm run build

# expose at port 3000
EXPOSE 3000

# default command is starting the server
CMD ["npx", "pm2-runtime", "start", "process.yml"]
