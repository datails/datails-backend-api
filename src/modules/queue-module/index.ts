import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { LoggerModule } from '../logger-module/index';
import { AppController } from '../../controllers/queue-controller';
import { BullConfigService } from '../../services/bull-config-service';
import { BullConfigModule } from '../bull-config-module';

@Module({
  imports: [
    BullModule.registerQueueAsync({
      imports: [BullConfigModule],
      name: process.env.QUEUE_NAME,
      useExisting: BullConfigService,
    }),
    LoggerModule,
  ],
  controllers: [AppController],
})
export class QueueModule {}
