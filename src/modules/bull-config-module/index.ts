import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { BullConfigService } from '../../services/bull-config-service';

@Module({
  imports: [
    BullModule.registerQueueAsync({
      name: process.env.QUEUE_NAME,
      useClass: BullConfigService,
    }),
  ],
  providers: [BullConfigService],
  exports: [BullConfigService],
})
export class BullConfigModule {}
