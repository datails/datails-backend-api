import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from '../../controllers/health-controller';
import { HealthService } from '../../services/health-service';
import { BullConfigModule } from '../bull-config-module';
import { BullConfigService } from '../../services/bull-config-service';

@Module({
  controllers: [HealthController],
  imports: [
    BullModule.registerQueueAsync({
      imports: [BullConfigModule],
      name: process.env.QUEUE_NAME,
      useExisting: BullConfigService,
    }),
    TerminusModule,
  ],
  providers: [HealthService],
})
export class HealthModule {}
