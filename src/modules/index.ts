import { Module, OnModuleInit } from '@nestjs/common';
import { BullModule } from '@nestjs/bull';
import { threadId } from 'worker_threads';
import { QueueModule } from './queue-module';
import { HealthModule } from './health-module';
import { LoggerModule } from './logger-module';
import { BullConfigService } from '../services/bull-config-service';
import { BullConfigModule } from './bull-config-module';

@Module({
  imports: [
    BullModule.registerQueueAsync({
      imports: [BullConfigModule],
      name: process.env.QUEUE_NAME,
      useClass: BullConfigService,
    }),
    QueueModule,
    HealthModule,
    LoggerModule,
  ],
})
export class AppModule implements OnModuleInit {
  onModuleInit() {
    console.log('MAIN: ', threadId);
  }
}
