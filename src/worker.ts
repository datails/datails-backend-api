import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { WorkerModule } from './worker/modules';

/**
 * the worker is meant to handle
 * all jobs stored inside our queu, so
 * we do not lose any data when pardot does
 * not react within a few seconds. Using this
 * approach we can post pardot forms
 * to our API and handle it in the background
 * using workers running in a different thread.
 */

async function bootstrapWorker() {
  const app = await NestFactory.create<NestFastifyApplication>(WorkerModule, new FastifyAdapter());
  await app.init();
}

export default bootstrapWorker;
