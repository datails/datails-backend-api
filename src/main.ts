import * as os from 'os';
import * as path from 'path';
import { Worker, SHARE_ENV, isMainThread } from 'worker_threads';
import bootstrapApp from './app';

// we turn the worker activation into a promise
const passJobToWorker = (): Promise<any> =>
  new Promise((resolve, reject) => {
    // utility method to only get the path
    // to the worker file
    const workerPath = () => path.join(__dirname, 'createProcesses.js');

    // create a new worker
    // and pass a db object
    // and the items to handle inside that worker
    const worker = new Worker(workerPath(), {
      // we want to share environment variables through all
      // workers, so we can increment the loader
      env: SHARE_ENV,
    });

    // if we receive a message of the worker
    // resolve
    worker.on('message', resolve);

    // if we receive an error
    // throw an reject
    worker.on('error', reject);

    // if the process exits and
    // the exit code is not 0,
    // throw an error
    worker.on('exit', (exitCode) => {
      if (exitCode !== 0) {
        reject(new Error(`An error occurred inside the worker: ${exitCode}`));
      }
    });
  });

// by default we use the maximum amount of CPU's as workers
const useWorkers = async (workers: number = os.cpus().length) => {
  if (isMainThread) {
    await bootstrapApp();
  }

  // create array with workers
  const promises = new Array(workers).fill(workers).map((_, index) => {
    return passJobToWorker();
  });

  try {
    // start all workers async
    await Promise.all(promises);
  } catch {
    process.exit(1);
  }
};

void useWorkers();
