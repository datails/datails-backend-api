import {
  Processor,
  Process,
  OnQueueActive,
  OnQueueError,
  OnQueueRemoved,
  OnQueueFailed,
  OnQueueCompleted,
} from '@nestjs/bull';
import { Job } from 'bull';
import { MailerService } from '@nestjs-modules/mailer';
import { threadId } from 'worker_threads';
import { AppLoggerService } from '../../services/logger-service';

@Processor(process.env.QUEUE_NAME)
export class WorkerProcessor {
  constructor(private readonly mailerService: MailerService, private readonly appLoggerService: AppLoggerService) { }

  @Process({
    name: process.env.JOB_NAME,
    concurrency: 5,
  })
  public async process(job: Job) {
    const { data } = job;

    this.appLoggerService.log(
      JSON.stringify({
        status: 'process',
        threadId,
        jobId: data.jobId,
      }),
    );

    try {

      const resp = await this
      .mailerService
      .sendMail({
        to: data.to,
        from: data.from,
        subject: data.subject,
        template: 'index',
        context: data,
      })

      if (resp.ok) {
        const text = await resp.text();

        // pardot returns always 200, but the errors are
        // listed within the response.
        if (text.includes('Please correct the following errors:')) {
          throw new Error(text);
        }
      } else {
        throw new Error(resp.statusText);
      }
    } catch (err) {
      // throwing an error will
      // remove the job to the failed queue
      // there we log the failures
      // the job will retry after x
      // miliseconds, defined in the module
      throw new Error(err);
    }
  }

  @OnQueueActive()
  public async onActive(job: Job) {
    this.appLoggerService.log(
      JSON.stringify({
        attempts: job.attemptsMade,
        jobId: job.id,
        name: job.name,
        status: 'onActive',
        threadId,
      }),
    );
  }

  @OnQueueCompleted()
  public async onCompleted(job: Job) {
    if (job.finishedOn) {
      this.appLoggerService.log(
        JSON.stringify({
          jobId: job.id,
          name: job.name,
          attempts: job.attemptsMade,
          value: job.returnvalue,
          status: 'onCompleted',
          threadId,
        }),
      );
    }
  }

  @OnQueueError()
  public async OnError(error: any) {
    this.appLoggerService.error(error.name, error);
  }

  @OnQueueFailed()
  public async OnFailed(job: Job, error: any) {
    this.appLoggerService.error(
      JSON.stringify({
        attempts: job.attemptsMade,
        jobId: job.id,
        name: job.name,
        value: job.returnvalue,
        status: 'onFailed',
        threadId,
      }),
      error,
    );
  }

  @OnQueueRemoved()
  public async OnRemoved(job: Job) {
    this.appLoggerService.log(
      JSON.stringify({
        attempts: job.attemptsMade,
        jobId: job.id,
        name: job.name,
        value: job.returnvalue,
        status: 'onRemoved',
        threadId,
      }),
    );
  }
}
