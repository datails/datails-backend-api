import { BullModule } from '@nestjs/bull';
import { Module, OnModuleInit } from '@nestjs/common';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { MailerModule } from '@nestjs-modules/mailer';  
import { threadId } from 'worker_threads';
import { LoggerModule } from '../../modules/logger-module/index';
import { WorkerProcessor } from '../services/worker.processor';
import { BullConfigService } from '../../services/bull-config-service';
import { BullConfigModule } from '../../modules/bull-config-module';

@Module({
  imports: [
    BullModule.registerQueueAsync({
      name: process.env.QUEUE_NAME,
      imports: [BullConfigModule],
      useExisting: BullConfigService,
    }),
    MailerModule.forRoot({
      transport: {
        host: 'smtp.sendgrid.net',
        port: 25,
        secure: false,
        auth: {
          accessToken: process.env.SMTP_API_KEY
        },
      },
      defaults: {
        from:'"datails" <datails.smtp@gmail.com>',
      },
      template: {
        dir: process.cwd() + '/templates/',
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    }),
    LoggerModule,
  ],
  providers: [WorkerProcessor],
})
export class WorkerModule implements OnModuleInit {
  onModuleInit() {
    console.log('WORKER', threadId);
  }
}
