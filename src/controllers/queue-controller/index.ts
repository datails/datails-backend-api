import { InjectQueue } from '@nestjs/bull';
import { Controller, Post, Body, HttpCode } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Queue } from 'bull';
import { QueueDTO } from './queue.dto';

@ApiTags('Send')
@Controller('send')
export class AppController {
  constructor(@InjectQueue(process.env.QUEUE_NAME) private readonly pardotQueue: Queue) {}

  /**
   * we only add a job to our queue here so we can return
   * the response as fast as possible to the client. We do not
   * need to wait for pardot to react, as we'll our scheduler handle
   * all items that we stored in the queue.
   */

  @ApiResponse({ status: 204, description: 'Store form data in queue.' })
  @HttpCode(204)
  @Post()
  public async sendForm(@Body() body: QueueDTO) {
    await this.pardotQueue.add(process.env.JOB_NAME, body);
  }
}
