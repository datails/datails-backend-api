import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsEmail } from 'class-validator';

export class QueueDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  readonly to: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  readonly from: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  readonly subject: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  readonly body: string;
}
