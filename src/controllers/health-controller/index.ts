import { Controller, Get } from '@nestjs/common';
import { HealthCheckService, HealthCheck, DiskHealthIndicator, MemoryHealthIndicator } from '@nestjs/terminus';
import { ApiTags, ApiResponse } from '@nestjs/swagger';
import { HealthService } from '../../services/health-service';

@ApiTags('Health')
@Controller('health')
export class HealthController {
  constructor(
    private health: HealthCheckService,
    private healthIndicator: HealthService,
    // private dns: DNSHealthIndicator,
    private disk: DiskHealthIndicator,
    private memory: MemoryHealthIndicator
  ) {}

  @ApiResponse({
    status: 200,
    description: 'Verifies if the app is healthy.',
  })
  @Get()
  @HealthCheck()
  check() {
    return this.health.check([
      () =>
        this.disk.checkStorage('disk-space', {
          path: '/',
          thresholdPercent: 5,
        }),
      () => this.memory.checkRSS('memory', 150 * 1024 * 1024),
      () => this.memory.checkHeap('heap', 150 * 1024 * 1024),
      () => this.healthIndicator.isQueueHealthy(),
    ]);
  }
}
