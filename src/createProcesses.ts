import { isMainThread } from 'worker_threads';
import bootstrapWorker from './worker';

async function createProcessPerApi() {
  if (!isMainThread) {
    await bootstrapWorker();
  }
}

void createProcessPerApi();
