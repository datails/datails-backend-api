import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { HealthIndicator, HealthIndicatorResult, HealthCheckError } from '@nestjs/terminus';
import { Queue } from 'bull';

@Injectable()
export class HealthService extends HealthIndicator {
  constructor(@InjectQueue(process.env.QUEUE_NAME) private pardotQueue: Queue) {
    super();
  }

  async isQueueHealthy(): Promise<HealthIndicatorResult> {
    try {
      const count = await this.pardotQueue.isReady();

      if (count.clients[0].status !== 'ready') {
        throw new Error(
          `No jobs in the total queue: ${count}.
           Active jobs: ${await this.pardotQueue.getActiveCount()}
           Failed jobs: ${await this.pardotQueue.getFailedCount()}
           Completed jobs: ${await this.pardotQueue.getCompletedCount()}
           
           workers: ${await this.pardotQueue.getWorkers()}
          `,
        );
      }

      return this.getStatus('bull', true);
    } catch (err) {
      throw new HealthCheckError('Bull ping failed', this.getStatus('bull', false, err));
    }
  }
}
