import { LoggerService } from '@nestjs/common';
import { WinstonModule } from 'nest-winston';
import * as winston from 'winston';

export class AppLoggerService implements LoggerService {
  private readonly logger: LoggerService = WinstonModule.createLogger({
    exitOnError: false,
    format: winston.format.combine(
      winston.format.label({ label: 'datails/backend/v1' }),
      winston.format.timestamp(),
      winston.format.simple(),
    ),
    transports: [
      new winston.transports.Console({
        debugStdout: true,
      }),
    ],
  });

  public error(message: string, trace: string) {
    this.logger.error(message, trace);
  }

  public warn(message: string) {
    this.logger.warn('warn', message);
  }

  public log(message: string) {
    this.logger.log('info', message);
  }
}
