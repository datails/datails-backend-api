import { BullModuleOptions, BullOptionsFactory } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';

const { REDIS_PASS, REDIS_HOST, REDIS_PORT } = process.env;

@Injectable()
export class BullConfigService implements BullOptionsFactory {
  createBullOptions(): BullModuleOptions {
    return {
      defaultJobOptions: {
        attempts: 15,
        backoff: 3600000,
        delay: 1000,
        timeout: 3000,
        removeOnFail: true,
        removeOnComplete: true,
        stackTraceLimit: 3,
      },
      redis: {
        host: REDIS_HOST,
        port: +REDIS_PORT,
        password: REDIS_PASS,
        showFriendlyErrorStack: true,
      },
    };
  }
}
