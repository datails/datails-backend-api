import {
  ArgumentsHost,
  BadRequestException,
  Catch,
  ExceptionFilter,
  HttpException,
  Inject,
  InternalServerErrorException,
  Logger,
  LoggerService,
  NotFoundException,
} from '@nestjs/common';
import { FastifyReply, FastifyRequest } from 'fastify';

@Catch(NotFoundException, InternalServerErrorException, BadRequestException)
export class HttpExceptionFilter implements ExceptionFilter {
  constructor(@Inject(Logger) private readonly logger: LoggerService) {}

  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const res = ctx.getResponse<FastifyReply>();
    const req = ctx.getRequest<FastifyRequest>();
    const status = exception.getStatus();
    const message = exception.message;
    const errors = (exception.getResponse() as any)?.message;

    this.logger.error(
      JSON.stringify({
        level: 'error',
        host: req.hostname,
        url: req.url,
        status,
        ip: req.ip || req.ips,
        headers: JSON.stringify(req.headers),
        message,
      }),
      exception.stack,
    );

    res.status(status);

    res.send({
      error: {
        errors: Array.isArray(errors) ? errors : [errors],
        message,
        status,
      },
    });
  }
}
