import 'dotenv/config';

import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import compression from 'fastify-compress';
import RateLimit from 'fastify-rate-limit';
import helmet from 'fastify-helmet';
import Redis from 'ioredis';
import { HttpExceptionFilter } from './filters/http-exception.filter';
import { AppLoggerService } from './services/logger-service';
import { AppModule } from './modules';

async function bootstrapApp(): Promise<void> {
  const isProduction = process.env.NODE_ENV === 'prod';

  // create nest app
  const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter({
    trustProxy: true,
  }));
  
  const logger = app.get(AppLoggerService);

  // get our logger singleton
  app.useLogger(logger);

  // set global prefix
  // note this prefix is dependend on
  // the API gateway prefix
  app.setGlobalPrefix('v1');

  // catch errors and transform the data
  // to our preferred format
  app.useGlobalFilters(new HttpExceptionFilter(app.get(AppLoggerService)));

  // validate incoming requests
  app.useGlobalPipes(
    new ValidationPipe({
      disableErrorMessages: isProduction,
    }),
  );

  // on acc we need the app to be exactly the
  // same as the production app, with the
  // exception of the ratelimiter, for load
  // test purposes
  if (isProduction) {
    // enable system hooks for health checking
    app.enableShutdownHooks();

    app.enableCors();

    app.register(helmet, {});

    // compress
    app.register(compression, { encodings: ['gzip', 'deflate'] });

    app.register(RateLimit, {
      timeWindow: 5 * 60 * 1000,
      max: 1000,
      redis: new Redis({
        host: process.env.REDIS_HOST,
        port: +process.env.REDIS_PORT,
        password: process.env.REDIS_PASS,
      }),
      errorResponseBuilder: (req, context) => {
        logger.error(
          JSON.stringify({
            level: 'error',
            host: req.hostname,
            url: req.url,
            statusCode: 429,
            ip: req.ip || req.ips,
            headers: req.headers,
            message: 'Rate limit exceeded, retry in 5 minutes.',
          }),
          context.after,
        );

        return {
          statusCode: 429,
          error: 'Too Many Requests',
          message: 'Rate limit exceeded, retry in 5 minutes.',
        };
      },
      keyGenerator: (req) => {
        return req?.ips?.find?.((ip) => ip) ?? typeof req.headers['x-real-ip'] === 'string'
          ? (req.headers['x-real-ip'] as string)
          : (req.headers['x-real-ip']?.find?.((ip) => ip) as string) ?? req.ip ?? req?.connection?.remoteAddress;
      },
    });
  }

  const options = new DocumentBuilder()
    .setTitle('Datails Backend')
    .setDescription('Datails API to handle backend processes.')
    .setVersion('1.0')
    .build();

  // initialize swagger docs
  const document = SwaggerModule.createDocument(app, options);

  // only expose swagger docs
  // on development
  SwaggerModule.setup('docs', app, document);

  // listen on port 3000
  await app.listen(3000, '0.0.0.0');
}

export default bootstrapApp;
